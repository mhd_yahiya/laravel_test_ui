import React, { useLayoutEffect, useState } from "react";
import DataTable from "react-data-table-component";

const UserData = ({ location, history }) => {
  const [userData, setUserData] = useState();

  useLayoutEffect(() => {
    if (global.cookie.get("user")) {
      const url = "http://localhost:8000/api/details";
      const data = "";

      const options = {
        method: "POST",
        headers: {
          Accept: "application/json",
          Authorization: "Bearer " + global.cookie.get("user").token
        },
        data: global.qs.stringify(data),
        url
      };

      global
        .axios(options)
        .then(res => {
          setUserData(res.data.status);
        })
        .catch(error => {
          console.log(error);
        });
    } else {
      history.push("/sign-in");
    }
    // eslint-disable-next-line
  }, [location]);

  const logOut = () => {
    global.cookie.remove("user");
    history.push("/sign-in");
  };

  const columns = [
    {
      name: "ID",
      selector: "id",
      sortable: true,
      center: true
    },
    {
      name: "Name",
      selector: "name",
      center: true
    },
    {
      name: "Email",
      selector: "email",
      center: true
    },
    {
      name: "Email verified at",
      selector: "email_verified_at",
      center: true
    },
    {
      name: "Created at",
      selector: "created_at",
      center: true
    },
    {
      name: "Updated at",
      selector: "updated_at",
      center: true
    }
  ];

  return (
    <>
      {userData ? (
        <div>
          <div className='flex p-8'>
            <button className='ml-auto' onClick={logOut}>
              Logout
            </button>
          </div>
          <div className='w-3/4 flex'>
            <div className='m-auto'>
              <DataTable
                title='User data'
                columns={columns}
                data={[userData]}
              />
            </div>
          </div>
        </div>
      ) : (
        <div>
          <div className='flex p-8'>
            <button className='ml-auto' onClick={logOut}>
              Logout
            </button>
          </div>
          <div className='h-screen flex items-center'>
            <div className='m-auto'>Fetching data...</div>
          </div>
        </div>
      )}
    </>
  );
};

export default UserData;
