import React from "react";

const Loader = () => {
  return (
    <>
      <div className='flex items-center h-screen'>
        <div className='m-auto'>Loading...</div>
      </div>
    </>
  );
};

export default Loader;
