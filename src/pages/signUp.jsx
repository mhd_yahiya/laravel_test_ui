import React, { useState } from "react";
import { Link } from "react-router-dom";
import { rules } from "../services/validator";
import useFormaHandlerHook from "../hooks/formHandler";

/* Validator schema */
const validatorSchema = {
  name: rules
    .string()
    .max(50)
    .required(),
  email: rules
    .string()
    .email({ minDomainSegments: 2 })
    .required(),
  password: rules
    .string()
    .min(6)
    .max(20)
    .required(),
  confirm_password: rules
    .required()
    .valid([rules.ref("password")])
    .error(errors => {
      errors.forEach(err => {
        err.message = `Passwords does not match`;
      });
      return errors;
    })
};
/* Validator schema */

const SignUp = () => {
  const [btnSpinner, setBtnSpinner] = useState(false);
  let fields;

  const {
    handleChange,
    values,
    errors,
    handleSubmit,
    handleErrors
  } = useFormaHandlerHook(
    {
      ...fields
    },
    validatorSchema
  );

  const verify = e => {
    e.preventDefault();

    handleSubmit()
      .then(() => {
        setBtnSpinner(true);

        global.axios
          .post("http://localhost:8000/api/register", { ...values })
          .then(res => {
            handleErrors({ api: "User registered" });
            setBtnSpinner(false);
          })
          .catch(error => {
            if (error) {
              handleErrors({ api: error.response.data.status });
            }
            setBtnSpinner(false);
          });
      })
      .catch(error => {
        console.log(error);
      });
  };

  return (
    <>
      <div className='flex items-center h-screen'>
        <form
          className='m-auto border rounded px-10 py-6 w-1/4'
          onSubmit={verify}
        >
          <h1 className='text-xl font-bold text-center'>Sign Up</h1>
          <div className='mt-4'>
            <label className='text-xs' htmlFor='name'>
              Name
            </label>
            <input
              id='name'
              type='text'
              className='w-full rounded-sm border p-3'
              placeholder='Name'
              name='name'
              value={values.name ? values.name : ""}
              onChange={handleChange}
            />
            <p className='text-red text-xs'>{errors.name}</p>
          </div>
          <div className='mt-4'>
            <label className='text-xs' htmlFor='email'>
              Email
            </label>
            <input
              id='email'
              type='text'
              className='w-full rounded-sm border p-3'
              placeholder='Email'
              name='email'
              value={values.email ? values.email : ""}
              onChange={handleChange}
            />
            <p className='text-red text-xs'>{errors.email}</p>
          </div>
          <div className='mt-4'>
            <label className='text-xs' htmlFor='password'>
              Password
            </label>
            <input
              id='password'
              type='password'
              className='w-full rounded-sm border p-3'
              placeholder='Password'
              name='password'
              value={values.password ? values.password : ""}
              onChange={handleChange}
            />
            <p className='text-red text-xs'>{errors.password}</p>
          </div>
          <div className='mt-4'>
            <label className='text-xs' htmlFor='confirm_password'>
              Confirm Password
            </label>
            <input
              id='confirm_password'
              type='password'
              className='w-full rounded-sm border p-3'
              placeholder='Confirm Password'
              name='confirm_password'
              value={values.confirm_password ? values.confirm_password : ""}
              onChange={handleChange}
            />
            <p className='text-red text-xs'>{errors.confirm_password}</p>
          </div>
          <div className='mt-6 flex'>
            <button className='text-sm bg-green px-4 py-2 text-white m-auto rounded-sm'>
              {btnSpinner ? "Signing up..." : "Sign up"}
            </button>
          </div>
          {Object.keys(errors).length ? (
            <p className='text-black text-xs text-center'>
              {errors.api ? errors.api : "Please review your inputs"}
            </p>
          ) : null}
          <div className='mt-4 text-sm text-center cursor-pointer'>
            <Link to='/sign-in'>Already have an account, Sign in</Link>
          </div>
        </form>
      </div>
    </>
  );
};

export default SignUp;
