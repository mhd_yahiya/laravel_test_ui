import React from "react";

const PageNotFound = () => {
  return (
    <>
      <div className='flex items-center h-screen'>
        <div className='m-auto'>Page not found.</div>
      </div>
    </>
  );
};

export default PageNotFound;
