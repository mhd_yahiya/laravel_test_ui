import { useState } from 'react';
import { validate } from '../services/validator';

function useFormHandlerHook(props, schema) {
    const [values, setValue] = useState(props);
    const [errors, setErrors] = useState({});

    const handleChange = (e) => {
        const { value, name } = e.target;

        setValue({
            ...values,
            [name]: value,
        });
    };

    const handleSubmit = () => {
        return new Promise((resolve, reject) => {
            validate(values, schema)
                .then(response => {
                    handleErrors({});
                    resolve({ isValidInputs: true });
                })
                .catch(error => {
                    handleErrors(error);
                    reject({ isValidInputs: false });
                });
        });
    };

    const handleErrors = errorBag => {
        setErrors(errorBag);
    };

    return { values, handleChange, errors, handleErrors, handleSubmit };
}

export default useFormHandlerHook;
