import Joi from '@hapi/joi';
import { upperFirst, uniqBy } from 'lodash';

const transformErrorText = unusualText => {
    unusualText = unusualText
        .toLowerCase()
        .replace(/["']/g, '')
        .replace(/[_]/g, ' ');
    return upperFirst(unusualText);
};

const filterErrors = errorBag => {
    let uniqueErrorBag = [...new Set(errorBag)];
    uniqueErrorBag = uniqBy(uniqueErrorBag, 'context.key');
    return uniqueErrorBag;
};

const validator = (data, schema) => {
    return new Promise((resolve, reject) => {
        const promise = Joi.validate(data, schema, {
            abortEarly: false,
            allowUnknown: true,
        });
        promise
            .then(value => {
                return resolve(value);
            })
            .catch(e => {
                const { details } = e;
                const filteredErrors = filterErrors(details);

                const filteredExceptions = filteredErrors.map(detail => {
                    return {
                        [detail.context.key]: transformErrorText(detail.message),
                    };
                });

                let exceptions = {};

                filteredExceptions.map(exception => {
                    exceptions = { ...exceptions, ...exception };
                    return exception;
                });

                return reject(exceptions);
            });
    });
};

export const rules = Joi;
export const validate = validator;
