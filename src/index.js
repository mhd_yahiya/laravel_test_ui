import React from "react";
import ReactDOM from "react-dom";
import * as serviceWorker from "./services/serviceWorker";
import Routes from "./Routes";
import "./styles/tailwind.css";
import cookie from "store";
import axios from "axios";
import qs from "qs";

global.axios = axios;
global.cookie = cookie;
global.qs = qs;

ReactDOM.render(<Routes />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
