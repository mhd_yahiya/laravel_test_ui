import React, { lazy, Suspense } from "react";
import { Router, Route, Switch } from "react-router-dom";
import { createBrowserHistory } from "history";

import Loader from "../src/pages/loader";
const SignIn = lazy(() => import("../src/pages/signIn"));
const SignUp = lazy(() => import("../src/pages/signUp"));
const UserData = lazy(() => import("../src/pages/userData"));
const PageNotFound = lazy(() => import("../src/pages/404"));
const history = createBrowserHistory();

const Routes = () => {
  return (
    <Router history={history}>
      <Suspense fallback={<Route component={Loader} />}>
        <Switch>
          <Route path='/sign-in' exact component={SignIn} />
          <Route path='/sign-up' exact component={SignUp} />
          <Route path='/' exact component={UserData} />
          <Route path='/404' exact component={PageNotFound} />
          <Route component={PageNotFound} />
        </Switch>
      </Suspense>
    </Router>
  );
};

export default Routes;
