module.exports = {
  theme: {
    colors: {
      transparent: "transparent",
      black: "#191919",
      white: "#ffffff",
      green: "#2daf3c",
      red: "#da270c"
    },
    screens: {
      xs: "239px",
      sm: "639px",
      md: "767px",
      lg: "1023px"
    },
    extend: {}
  },
  variants: {
    visibility: ["responsive", "hover", "focus"],
    overflow: ["responsive", "hover", "focus"],
    backgroundColors: ["responsive", "hover", "focus"]
  },
  plugins: []
};
